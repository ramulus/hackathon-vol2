﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookingApi.Infrastructure.Db.Migrations
{
  public partial class AddPlanTable : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "Plan",
          columns: table => new
          {
            Date = table.Column<DateTime>(type: "date", nullable: false),
            WorkingState = table.Column<string>(type: "text", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Plan", x => x.Date);
          });
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(name: "Plan");
    }
  }
}
