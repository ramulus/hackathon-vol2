﻿using BookingApi.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace BookingApi.Infrastructure.Db
{
  public class BookingApiDbContext : DbContext
  {
    public DbSet<ParadoxBookings> ParadoxBookings { get; set;}
    public DbSet<Plan> Plans { get; set; }

    public BookingApiDbContext(DbContextOptions options) : base(options)
    {
    }
  }
}
