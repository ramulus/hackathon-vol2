﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BookingApi.Infrastructure.Models
{
  [Table("Plan")]
  public class Plan
  {
    [Key]
    [Required]
    [JsonPropertyName("date")]
    [Column(TypeName = "date")]
    public DateTime Date { get; set; }

    [Required]
    [JsonPropertyName("working_state")]
    public string WorkingState { get; set; }
  }
}
