﻿using BookingApi.Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BookingApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class PlansController : ControllerBase
  {
    /// <returns>List of plans. If there is no single plan for a given email, return empty collection.</returns>
    /// <response code="400">If email or dates are incorrect</response> 
    /// <response code="200">List of plans</response>
    [HttpGet("{email}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Plan>))]
    [Produces("application/json")]
    public IEnumerable<Plan> Get(string email, string startDate, string endDate)
    {
      return new List<Plan> {
        //TODO: Needs to convert to proepr dateTime format yyyyMMdd
        new Plan { Date = new DateTime(2021,5,28), WorkingState = "remote" },
        new Plan { Date = new DateTime(2021,5,26), WorkingState = "remote" }
      };
    }
  }
}
