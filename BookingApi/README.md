# BookingApi

## Background

This project has been set up based on docker containers. This allow each of us to develop changes without a worry of breaking DB Server and makes developers ENV equal each other. [Docker Get Started](https://docs.docker.com/get-started/overview/)

## Requiremnts

In theory you can write this project in any platform and any IDE or editor, docker should allow you to do that. However I have tested and used in Windows with VisualStudio 2019.

### Generic

Mostly usfeul if you want to coding in different OS

1. Docker for given OS with Enging **20.10.6**
2. Compose **1.29.1**
3. In theory docker container should contain all yopu need in terms of .net, but just in case all backend has been set up **.net 5**

### Windows and VS

Mostly usfelu when you want to coding in Windows and using VS

1. You need to install [WSL2](https://docs.docker.com/docker-for-windows/install/#what-to-know-before-you-install) **Please note** that you will need Windows 10 64-bit: Home, Pro, Enterprise, or Education, version 1903 (Build 18362 or higher).
2. The latest VS 2019 Community edition, please verify if you have the all updated installed
3. Few GB of free disk spaces on C drive

## Before coding

1. **Run docker-compose**. I am quite intresed how it will works with VS so if it is not problem run it by picking up `docker-compose` as startup application and `Docker Compose` as Debug option. This additionally start app for debuging (open browser with swagger doc), which is nice benefit, but you can press stop if you just want to set up it. However if you are prefer to run containers in more ordinary and predictable way you can:
    1. Open your console in admin mode (ConEmu Rulez!)
    2. Type: `docker-compose up` it should start 2 containers. One for BookingApi and another for postgres
    3. Type: `docker ps` to verify that
    4. Open web browser and navigate to: `http://localhost:5005/swagger/index.html`. You shoudl be able to see swagger doc for our api, which is running inside a docker. Please note that to run container with a new code changes yuou need to rebuild it: `docker-compose up -d --build bookingapi`
    2. To remove container you can:
       1. Rebuild solution in VS . Yes this will remove all containers :)
       2. Type: `docker-compose down`
    3. To stop container you can:
       1. Stops all containers: `docker-compose stop`
       1. Stop single container `docker-compose stop <container_name>`
3. **Run DB migration**: Open Package Manager Console and type: `update-database`

## Coding

1. To Add new interface as class collaborator (composition) you need bind it in DI configuration. In our case default DI provided by .net, which mean adding `services.AddScoped<IInterfaceName, ClassName>()` to `Startup`

## Working with EF migrations

The are two reasons why I decide to write this sections:
1. EF migrations works differently compering to other frameworks e.g. ruby on rails - it is not immutable, snapshots/schema do not reflect migration/db state. More below.
1. I do not have much expirience with them

In short if you are EF migration expert, then you do not have to go through, this and save your time on my typos :)

**Potential issue with keeping snapshot in sync with code changes**

1. *Dzakets first migration issue*:
   1. I have add migration
   2. Snapshot has been generate
   3. I have changed DBContext and model
   4. However, snapshot was out of sync with them. I was unable to regenerate this auto-generated files
5. *Common dev problem*. It is too much to explain so I will just say that this is alwyas happening when two devs adding changes to db in the same time. Details and solutions can be found below, but in short the advise is to add temp and empty migration:
  1. https://www.mikee.se/posts/corrupted_model_snapshot_in_ef_core_20210117 
  4. https://docs.microsoft.com/en-us/ef/ef6/modeling/code-first/migrations/teams?redirectedfrom=MSDN 

**Regarding my dummy issue and to understnad a bit better how EF migration works:**
1. There two snapshots: one related to dbcontext and one per each migration
2. Neither of them reflect DB state nor migration class code like in e.g. ruby on rails schema
3. They directly reflect to DBContext and model classes code. As far as I understood there is no way, to customize migration and update snapshot by this chanegs. Saying that, they maybe some tricks
1. Think about migration like about changes applied to db and snapshot like a code/model changes, which EF use to compare models and scaffolding new migration. For me it looks like snapshot do not have impact on db.
1. In theory all snapshots, migrations and models should be in sync. However there is only one moment to sync them - when you adding new migration. Whatever you do after that, doesn't reflect your snapshot changes, unless you will add new migration.
1. There is a two ways of fixing that depend your shared db state e.g. DB on PROD
   1. If you like me and you and apply changes just to local DB, then you can try revert db changes (see below), next change your model/dbcontext and add migration one more time.
   1. If your changes has been already applied to shared db e.g. on PROD, you do not have much choice and add yet another migration which fixed previous one. You cannot remove existing migration! 

**EF comments:**
1. To add new DB migration open Package Manager Console and type: `add-migration <NamOfMigration> -OutputDir "Infrastructure\Db\Migrations"`
1. List migrations: `Get-Migration`
1. Apply migration to your db instance: `Update-Database`
3. To Revert changes:
   1. First rivert change on your DB instance (actually apply specific migrations): `Update-Database -Migration:"<full_migration_name>"`
   1. Remove last migration file and revert last changes on shnapshot: `Remove-Migration`. Please not that this operation do not make any changes to your instance of db!
   1. Verify if changes has been appied to db correctly:
      1. "Connect" to your postgres container: `docker exec -it postgres /bin/sh`
      2. Connect to db: `psql -U '<user_name>' -d bookings`
      3. List content of internal migration table: `select * from "__EFMigrationsHistory";`
      4. If you do not like console you can install pgAdmin applciation to manage your postgres db.

Dzaket do not approved EF DB migrations :).

