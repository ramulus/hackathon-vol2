import { userActionTypes } from "../actions/userActions";

export default function userReducer(state = { email: "" }, action) {
  switch (action.type) {
    case userActionTypes.setEmail:
      return { email: action.payload };
    default:
      return state;
  }
}
