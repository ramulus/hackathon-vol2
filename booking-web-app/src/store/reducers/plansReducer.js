import { plansActionTypes } from "../actions/plansActions";

export default function plansReducer(state = [], action) {
  switch (action.type) {
    case plansActionTypes.set:
      return [...action.payload];
    default:
      return state;
  }
}
