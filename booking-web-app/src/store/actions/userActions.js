export function setEmail(email) {
  return {
    type: userActionTypes.setEmail,
    payload: email,
  };
}

export const userActionTypes = {
  setEmail: "user/setEmail",
};
