import ky from "ky";
import { setPlans } from "./plansActions";
import { push } from "connected-react-router";
import { setEmail } from "./userActions";

export function fetchPlans(email) {
  return async (dispatch) => {
    const plans = await ky.get(`http://localhost:5005/plans/${email}`).json();

    dispatch(setPlans(plans));
    dispatch(setEmail(email));
    dispatch(push("/plans"));
  };
}