export function setPlans(plans) {
  return {
    type: plansActionTypes.set,
    payload: plans,
  };
}

export const plansActionTypes = {
  set: "plans/set",
};
