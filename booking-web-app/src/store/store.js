import { configureStore } from "@reduxjs/toolkit";
import plansReducer from "./reducers/plansReducer";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { createHashHistory as createHistory } from "history";
import userReducer from "./reducers/userReducer";

export const history = createHistory();

export default configureStore({
  reducer: {
    plans: plansReducer,
    user: userReducer,
    router: connectRouter(history),
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(routerMiddleware(history)),
});
