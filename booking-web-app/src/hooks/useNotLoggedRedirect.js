import { push } from "connected-react-router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export function useNotLoggedRedirect() {
  const dispatch = useDispatch();
  const email = useSelector((state) => state.user.email);

  useEffect(() => {
    if (email === "" || email === undefined || email === null) dispatch(push("/"));
  }, [email, dispatch]);
}
