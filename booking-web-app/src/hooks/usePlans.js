import { useSelector } from "react-redux";
import { PlanStoreFormatMapper } from "../domain/mappers/PlanStoreFormatMapper";

export function usePlans() {
  const planFactory = new PlanStoreFormatMapper();

  return useSelector(state => state.plans).map(planFactory.mapFromStoreFormat);
}