import { ArgumentError } from "../errors/ArgumentError";
import { Plan } from "../objects/Plan";

export class PlanStoreFormatMapper {
  mapFromStoreFormat({ date, working_state }) {
    return new Plan(hydrateDate(date), working_state);
  }

  mapToStoreFormat(plan) {
    if (!(plan instanceof Plan)) throw new ArgumentError("'plan' should be instance of Plan.");

    const { date, workingState } = plan;

    return {
      date: formatDate(date),
      working_state: workingState,
    };
  }
}

function hydrateDate(date) {
  const year = date.substring(0, 4);
  const month = date.substring(4, 6);
  const day = date.substring(6);

  return new Date(year, month - 1, day - 1);
}

function formatDate(date) {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return `${year}${month}${day}`;
}
