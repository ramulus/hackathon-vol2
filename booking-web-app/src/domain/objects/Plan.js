import { ArgumentError } from "../errors/ArgumentError";

export class Plan {
  constructor(date, workingState) {
    if (!(date instanceof Date)) throw new ArgumentError("'date' should be instance of Date.");
    if (typeof workingState != "string") throw new ArgumentError("'workingState' should be a string.");

    this._date = date;
    this._workingState = workingState;
  }

  get date() {
    return new Date(this._date);
  }

  get workingState() {
    return this._workingState;
  }
}
