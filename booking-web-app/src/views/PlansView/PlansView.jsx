import React from "react";
import { useNotLoggedRedirect } from "../../hooks/useNotLoggedRedirect";
import { usePlans } from "../../hooks/usePlans";

export function PlansView() {
  useNotLoggedRedirect();

  const plans = usePlans();

  return (
    <>
      {plans.map((plan) => (
        <div key={plan.date}>{formatPlan(plan)}</div>
      ))}
    </>
  );
}

function formatPlan(plan) {
  return `Date: ${plan.date.toLocaleDateString()}, State: ${plan.workingState}`;
}
