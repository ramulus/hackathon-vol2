import React, { useState } from "react";
import Content from "./Content";
import { useDispatch } from "react-redux";
import { fetchPlans } from "../../store/actions/httpActions";

export function LandingPageView() {
  const [email, setEmail] = useState("");
  const dispatch = useDispatch();

  const onSubmit = async (event) => {
    event.preventDefault();
    dispatch(fetchPlans(email));
  };

  return <Content onEmailChange={(event) => setEmail(event.target.value)} onSubmit={onSubmit} email={email} />;
}
