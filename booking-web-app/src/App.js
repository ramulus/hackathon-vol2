import React from "react";
import { Route, Switch } from "react-router-dom";
import { LandingPageView } from "./views/LandingPageView/LandingPageView";
import { PlansView } from "./views/PlansView/PlansView";
import store, { history } from "./store/store";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";

function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/plans">
            <PlansView />
          </Route>
          <Route path="/">
            <LandingPageView />
          </Route>
        </Switch>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
