# Booking WebAPP

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Background

This project has been set up based on docker containers. This allow each of us to develop changes on web project without worry about proper components version or their set up e.g. node. [Docker Get Started](https://docs.docker.com/get-started/overview/)

## Development environment setup with Docker

### Generic

Mostly usfeul if you want to coding in different OS

1. Docker for given OS with Enging **20.10.6**
2. Compose **1.29.1**
3. In theory docker container should contain all yopu need in terms of .net, but just in case all backend has been set up **.net 5**

### Windows and VS

Mostly usfelu when you want to coding in Windows and using VS

1. You need to install [WSL2](https://docs.docker.com/docker-for-windows/install/#what-to-know-before-you-install) **Please note** that you will need Windows 10 64-bit: Home, Pro, Enterprise, or Education, version 1903 (Build 18362 or higher).
2. The latest VS 2019 Community edition, please verify if you have the all updated installed
3. Few GB of free disk spaces on C drive

## Before coding

1. Open console e.g. **ConEmu**, Powershell and navigate directly under web project in my case `F:\dev\sources\hackathon-vol2\booking-web-app`
1. Run: `docker-compose up -d` to build and start container. Please note that in this case we have single container, it is just easier to use docker-compose.
1. Verisy if container is working: `docker ps`
1. Remote to your container bash: `docker exec -it booking-web-app  /bin/sh`. Now you are inside container
5. Start npm server: `npm start`. It should takes about 10 sec, after all you should see two urls.
6. Open your browser and navigate to: `http://localhost:3000/` and you should be able to see react start page.

## Coding
1. You should change react app files locally using what ever editor you like. And fancy volume feature from docker should make sure that your changes are **sync** with docker container. **Please note that you have to save files first**
1. It is even more cool. If you already have started server `npm start` you do not have to reset it since your chanegs should be *realoaded*. Sexy!
1. Above is not realted to npm modules dir: `/booking-web-app/node_modules`. It is because they slow down server start up to about 2 mins vs few sec. So if you want to **add new module** you have call `npm install` on **docker container** not local machine! That is very important.

## Issues
1. If at some point your docker configuration will eat tones of GB disk space, youc can right click on docker ico, next choice `Troubleshoot` and `Clean / Purge data`. Please note that this will remove all: images, containers and even volumes!
1. If your machine can not work because `Vmmem` taking out your RAM, you can try [this workaround](https://github.com/microsoft/WSL/issues/4166#issuecomment-526725261) and restart your machine. 
