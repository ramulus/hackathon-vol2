# Let's do some hacking

A goal of the hackathon is creating an application that makes life at Acturis easier. Ideally, lives of all or most of us. Indirect goal is to meet, get familiar with new technologies, methodologies, exchange our knowledge and experience. Besides, it's good to meet, talk, grab a beer and have fun after work! 

## Technical requirements

There are no many things worth to list in advance and a lot if stuff depend on `pre` meeting, but we think it will be good to establish some basic rules:

1. To simplify stuff for all members all code should based on GIT repositories.
1. You may decide to you use whatever repo you want, but we encourage you to use this "Hacking" project to have all everything in the same place.
1. It will be nice and easier if you will be able to define some basic style/coding rules and share with other colleagues in advance. In this repo you can find two files: `Settings.Stylecop` and `.editorconfig`, which we are using to share some basic c# coding rules.
1. It will be nice to add a brief description of your idea to README
1. By the end of hackathon, your solution may not be fully finished e.g. may have a dummy integration with external services, but it should be ready to demo
1. It will be nice to give other teams a way to try your app, in such case you may think of some free hosting plan e.g. in Heroku
2. Please keep in mind that this repo is **public one**.

## Hackathon Wiki

More information to be found on [Wiki](https://bitbucket.org/ramulus/hackathon-vol2/wiki/Home)

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).